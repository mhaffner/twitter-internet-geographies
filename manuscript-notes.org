- The data avalachne
- Concerns over data quality
- 
- Types of information in tweets
- Death by end user license agreement; we agree to things we don't
  realize, things we wouldn't agree with in the first place, but once
  we hit "accept" and start using, we can't stop.


- NoSQL and distributed computing models, data duplication rather than
  table joins. 
- challenges of producing real-time accurate data sets
- false positive vs. false negative

- Beyond the geotag
For this reason, it is perhaps beneficial to conceive of
explicit vs. implicit contributions on a continuum rather than in
discrete categories. Additionally, it is useful to separate the type
of geographic contribution from the user's level of compliance
(SOURCE: NSF workshop). A contributor to OSM is highly involved in
that they are formally participating (i.e. volunteering), while a
Twitter user can contribute geographic features (i.e. a geotag)
without even realizing it.

While X estimates that only 70%XX of self defined profile
locations are accurate or up to date based on other tweet content,
blah blah blah., a comibination a methods may be helpful in 

- 20% of tweets reference location directly or
indirectly.

While location data in Twitter itself is explicit, the action of the
user to tag a location is more implicit...

- Digital divide

- Studying spatial patterns, potential supplement (or replacement) for
  conventional data sources. Crisis situations, revealing patterns of
  disparity.
- Digital divide.
 
Twitter is more open to the academy than
other social media platforms in terms of the way that it provides
data, but the company still makes use targeted advertisements

- The big data paradox: once you chop it up into useful pieces,
  datasets are often quite small; Shelton (2014)
** How Twitter has been used by users?
- Racial protest (organized protest in general)
- Policital movements (Arab Spring)
- Lurking on other users rather than producing content.

** How will it be used by geographers in the future? (Maybe put this in conclusion)
- Location-based sentiment analysis cite:Stojanovski2016
- Smart cities and city planning

** From critical GIS to emancipatory perspectives -> eliciting patterns of disparity

- Shelton's peice on Sandy?
- Emily's articles
- 

** Critique vs. utility
- CyberGIS and spatial data science (Wang 2016)
- Need to check out cite:Shelton2017
- 

** Topical analysis of text (using tweet text)
- Beer tweets
- Linguistic variation cite:Huang2016
- Twitter topics in London cite:Lansley2016
- Longley et al. appropriate here???
 
Perhaps
the most researched VGI platform is Open Street Map (OSM), a

* Future directions 
Much of the work on Twitter from a geographic perspective is about
nature of the data, rather than simply applying the data to answer
some question. This is largely because there are still many unknowns
about the data itself, and often studies raise more questions than
they provide answers.

** Emerging/untapped platforms
- China's Sina Weibo (check recent issues of transactions in GIS)
- Instagram?

** Places outside of the US
- Though its less popular in other countries...

For example, a spatial
index speeds up MongoDB geo-queries but is not essential, whereas
Elasticsearch requires a spatial index for any geo-query.

** Difficult in tying characteristics to users -> inferring home locations
- Methods are improving cite:Lin2018 . 
- Longley et al. 2015 (??)
- Doing this would be helpful to know the characteristics of users
  better, yet ->
- Doing this raises questions about the ethics of such practices -> 10
  responsible rules for big data research, geoprivacy manifesto
- This stuff contrasts with several of Zook et al.'s cite:Zook2017 "Ten
  simple rules for responsible big data research" and Kebler and
  Mackenzie's (2018) "geoprivacy manifesto". 
- Dobson warned of this in 2007.
- Though Twitter deletes location information after two weeks, they do
  not have control over people who are collecting data, using public API.
Twitter deletes location information from tweets after two weeks, yet
they can do nothing about third parties who have used their APIs to
harvest location data store it on privately owned drives.

* Other things to include
** Combining datasets

Will geolocated Twitter data be a force for good or ill? Many (Harvey,
Zook, Shelton) cast it in a positive light, while others take a more
cautionary approach. Elwood calls for a new conceptualization of
privacy..... Stephens says men are mapping and women are being mapped.  

- 

** Types of location information in tweets
- Ambient geospatial information
- How to extract location information cite:Stock2018.

Platforms like Ushahidi exist for combining data sets, but they are
all utter garbage.

