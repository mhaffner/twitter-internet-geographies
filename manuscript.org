#+LATEX_HEADER:\usepackage[utf8]{inputenc}
#+LATEX_HEADER:\usepackage{charter}
#+LATEX_HEADER:\linespread{2}
#+LATEX_HEADER:\usepackage[margin=1.0in]{geometry}
#+OPTIONS: ^:nil toc:nil enquote:t language:am
#+TITLE: Twitter
* Introduction
** Overview
Twitter is a unique social media platform. As of 2018, it has around
335 million active users cite:Twitter2018a, but volume alone is not
what makes it noteworthy. Facebook and Instagram have more followers,
but Twitter's relative openness and accessibility have made it of
special interest, particularly within the academy. Twitter's public
application programming interfaces (APIs) make it easy to extract
data, and users' profiles are less frequently "locked", or made
private, than other platforms cite:Haffner2018. These factors,
combined with the mobile nature of users and their ability to tag
their location, have made Twitter the target of much geographic
research. A recent meta-analysis of articles that utilize location
information in social media posts reveals that Twitter dominates the
academic landscape, with over 54% of studies utilizing this platform,
while the next most used platform, Flickr, comprises only 20%
cite:Stock2018.

At the same time, Twitter is utilized in geography more than any other
domain cite:Williams2013. Applications have been wide ranging and
include the study of content variation over space, linguistic
variation, natural disaster reporting, and socio-spatial disparities.
Yet, using geolocated Twitter data appropriately remains a challenge.
Many questions surround its applicability, with concerns over
demographic and spatial biases, representativeness, and issues of
privacy. In this chapter, I first discuss history of location tagging
on Twitter and geographic web contributions. Then, I discuss how
geolocated Twitter data has been conceptualized, ways in which has
been used, and challenges of using Twitter as a data source.

** History of location tagging
In August of 2009, Twitter developed a feature allowing users to
attach their location to a post, often termed "geotagging"
cite:Twitter2009. The feature augmented the data streamed to the
Twitter APIs, and later the way that tweets appeared on users'
profiles, allowing developers to filter tweets by location. Users for
the first time could view content relevant to a particular place.
While this was novel at the time, initial location tagging from the
user's perspective was cumbersome as they could only enable location
by navigating through multiple tiers of menus within their profile
settings. Users only had the option to tag their location using their
device's global positioning system (GPS) system, broadcasting their
precise location as a latitude-longitude coordinate pair. Location
tagged posts remained enabled until manually disabled, making it
difficult to geotag on a tweet-by-tweet basis. In turn, this
configuration also made users vulnerable to forgetting that the
location was enabled, causing them to unintentionally broadcast their
location with every post.

The problematic nature of Twitter's location sharing feature was was
notably exposed when Adam Savage, host of the popular TV show
/Mythbusters/, posted a geotagged picture of his car on Twitter with a
caption containing "Now it's off to work...", thus revealing his
precise home location to thousands of followers at the exact moment
when he was leaving the premises cite:Murphy2010. While ethical and
privacy concerns still abound, Twitter has significantly modified it's
location-sharing options, among other features, since then. Notable
changes came in 2015 when Twitter announced they would be partnering
with the location tagging platform Foursquare, allowing users users to
tag a more ambiguous "general" location, which could be a city, state,
or neighborhood cite:Twitter2018b. Alternatively, users can now tag a
more specific location like a restaurant, business, or parks without
using precise latitude-longitude coordinates. This change also added a
location icon to every tweet, making it possible to geotag individual
tweets and rendering it unnecessary to alter profile settings first.

* Crowdsourced geographic information
Frequent changes to its location sharing options have made Twitter a
difficult platform to conceptualize, though at a basic level it is
part of a larger trend in web contributions that have dismantled the
traditional hierarchical structure of the internet. Whereas "Web 1.0"
was characterized by Netscape, web encyclopedias, and top-down
production, Web 2.0 is characterized by Google, Wikipedia, and
user-generated content, which now dominate the web cite:OReilly2005.
Web 2.0 paved the way for geographic contributions on the internet,
which Goodchild cite:Goodchild2007 termed volunteered geographic
information (VGI) in his seminal paper /Citizens as sensors: the world
of volunteered geography/. This phenomenon sparked a wave of research
in the contributions of geographic features by non-experts, including
platforms such as OpenStreetMap (OSM), Wikimapia, eBird, and Twitter.

** The challenge of classification: VGI or something else? 
A number of different labels have been put forth in classifying
Twitter, among other social media platforms, within the geospatial
web. Terms include location-based social network (LBSN)
cite:Evans2015,Evans2015a; georeferenced social media
cite:Shelton2014; geosocial media cite:Shelton2015; geosocial
services, in reference to location check-in services cite:Zickuhr2013;
and location-based social media (LBSM)
cite:Wilken2014,Zhao2018,Evans2015a,Haffner2018,Schwartz2015,Yuan2018,
with this last term seemingly dominant. That said, situating its role
within the broader network of crowdsourced geographic content is
difficult. Early studies using geolocated Twitter data considered it a
type of VGI, but recently scholars have questioned the amount of
"volunteerism" in geotagged tweets.
 
Harvey cite:Harvey2013 makes a careful distinction between VGI and
what he calls "contributed" geographic information (CGI), that is,
locational information which is not necessarily provided willingly.
Harvey cite:Harvey2013 also differentiates between opt-out services --
those that enable location by default and require the user to disable
them manually -- and opt-in services, which disable location features
by default. While Twitter does not enable location tagging
automatically, it does not cleanly fit into the opt-in category. Once
a user geotags a post, their tweets remain location-enabled until the
location icon is deselected. This typically involves only the click of
a button, but users can still unintentionally broadcast their
location, as mentioned in the earlier example of Adam Savage. 

Beyond this, Twitter's internal use of location complicates
categorization. The company uses location-based advertisements based
on a user's web IP address and GPS signal, regardless of whether or
not a user geotags their posts cite:Twitter2018c. Additionally,
advertisements can be gender-, age-, and language- targeted. Though
users can disable targeted advertisements, it is /enabled/ by default
and therefore should be considered opt-out. Even if other Twitter
/users/ cannot view a person's location from a tweet, the company and
those with whom they wish to share data still have access to location
information of the users. Twitter is forthright about this practice --
even encouraging -- as they emphasize that targeted advertisements
enhance the user's experience cite:Twitter2018d. From Twitter's
perspective, effective advertising is desirable as it engenders
lucrative financial partnerships with other organizations. Such
subtleties blur the lines between opt-in and opt-out services, further
complicating classification of geolocated Twitter data.

Another salient challenge lies in conceptualizing the nature of
spatial information in tweets since an abundance of locational
information is available -- far more than a singular coordinate pair
determined by the user's device. The JavaScript Object notation (JSON)
object created when a tweet is produced contains numerous fields, with
many directly or indirectly referring to location. Two user-defined
(i.e. manually selected) locational references attached are attached
to the user's account: (a) a location field which provides a link to
content from other users claiming that place and (b) a timezone,
selected when user creates an account and only exposed through the
APIs. Additionally, each user is assigned a profile language based on
the language they used to access Twitter with during account creation.
Though not explicitly location-based, language is indicative of a
user's characteristics and cultural preferences.

** Implicit vs. explicit spatial information
On an individual level, tweets can reference location in numerous ways
with varying degrees of spatiality, from explicit to implicit
cite:Elwood2012,Graham2013. The contribution of a building footprint
on OSM, for instance, is clearly an explicitly geographic
contribution, but Twitter contributions are more fluid. The underlying
spatial information produced by a user who formally geotags a tweet is
explicit, but other references to location, such as mentioning a city
or restaurant or attaching a photograph, tend towards implicit spatial
information. These more benign references to location, which are
common in social media, are termed "ambient geospatial information"
cite:Stefanidis2013. Indeed, cite:Humphreys2012 finds that roughly 20%
of tweets reference an individual's location in some way, though the
number of tweets formally geotagged is very small cite:Leetaru2013.

* How Twitter is used
Geolocated Twitter data has been used in a wide array of applications,
and motivations for its use have been multifaceted. While early
applications were simple, analyses have become more complex,
coinciding with Crampton et al.'s cite:Crampton2013 call to move
"beyond the geotag" -- a petition to study connections, networks, and
relationships; supplement Twitter with other data sources; and
incorporate time into analyses, rather than mapping a static snapshot
of points. 

One principal way geolocated Twitter data has been used is in studying
differential content production over space
cite:Zook2014,Lansley2016,Haffner2018b,Shelton2015, in which social
media discourse often complicates traditional or assumed boundaries.
The relationship between country and language in Europe, for instance,
is blurred by overlapping and disjunct language clusters of geolocated
tweets throughout the continent. (see Fig. 1). Similarly, the commonly
held notion of the "9th Street Divide" in Louisville, Kentucky is
rendered more fluid by noteworthy references to the term "ghetto" on
both sides of the dividing line cite:Shelton2015. The use of
"#AllLivesMatter" as a counter-protest narrative on Twitter is
challenged by the unexpected correlation coefficients of
municipalties' racial demographics, in which percent Black has a
positive relationship with the number of tweets containing this
phrase.

Social media data are continually streaming, as opposed to
conventional data sources like those from the U.S. Census, which are
collected only periodically and are expensive when done so. Since
users are mobile and can tag locations from a location-enabled device,
a logical application is the study of day-to-day mobility patterns and
urban dynamics, as a potential replacement to sources like the
longitudinal origin-destination employment statistics (LODES) or other
datasets collected using travel diaries (should this go here?).
Observing the locations of geolocated tweets over different daily time
periods (e.g., day vs. night), for instance, produces noticeably
distinct spatial patterns (see Fig. 2). 

Beyond simple visual methods, Lee, Gao, and Goulias cite:Lee2016
predict trip generation patterns in Los Angeles using geolocated
Twitter data collected over two days, finding comparable results to
the city's travel demand forecasts. Over a longer period of time,
Abbasi et al. cite:Abbasi2015 build data dictionaries to differentiate
tourist from resident geolocated tweets and estimate trip purpose in
Sydney. Despite such methodological advancements, Eggermond et al.
(SOURCES) find that predicting users' home locations is still
difficult, restricting the applicability of such methods. Indeed, few
users tag their home location in posts cite:Haffner2018, so mobility
patterns on Twitter may not be representative of daily commuting
patterns. Therefore, such methods may be more effective in explaining
popular activity locations at various times of the day, or simply
activity-to-activity, work-to-activity, or activity-to-work mobility
patterns rather than home-to-work or work-to-home patterns.

The area arguably receiving the most attention in the use of
geolocated Twitter data lies in hazards and disasters research. Due to
the chaotic nature of these evens and the difficulty in appropriately
assessing need in such situations, social media data have the
potential to save lives, improve response times, and target most
vulnerable areas quickly. Augmenting social media data with an
open-ended survey of Twitter users, Acar and Muraki cite:Acar2011 find
that people commonly use Twitter to inform followers of their safety.
On the other hand, hashtags are used to request emergency evacuations
for friends and family. #HarveySOS, #HarveyRescue, and #HoustonRescue,
for example, were heavily used following Hurricane Harvey
cite:Yang2017.

Many studies have studied the spatial patterns of production
post-disaster in an attempt to better understand how users report
during events. Examples include the 2012 Horsethief Canyon Fire in
Wyoming cite:Kent2013, Typhoon Haiyan in the Philippines
cite:Takahashi2015, and the Great Tohoku earthquake in Japan
cite:Acar2011, among others. A key challenge in disaster situations
lies in quickly synthesizing information, and while algorithms have
been proposed for sorting disaster-related Twitter data in based both
time and urgency cite:Yang2017, compiling time-sensitive crowdsourced
data in a meaningful way remains an arduous task. Rather than used on
their own, a more effective approach may be combining social media
data with conventional data cite:deAlbuquerque2015 or other
crowdsourced datasets.

* Challenges of using Twitter data
** Demographic and spatial bias 
The limitations mentioned earlier only scratch the surface of
challenges faced by using Twitter data for geographic research. Though
all data have limitations, crowdsourced sources present significant
concerns over representativeness, which in turn impairs generalization
of results. That said, Twitter appears more representative
demographically than other social media platforms. In a study of
college students, Haffner et al. cite:Haffner2018 find no
statistically significant differences in the use of Twitter and
geotagging on Twitter by gender and race. Similarly, in a larger study
of the general population, only small differences in Twitter use are
found between racial groups: 20%, 26%, and 20% for Hispanic, Black,
and white respondents, respectively cite:Smith2018. Further, while only a
1% difference in Twitter adoption is found between women and men
cite:Smith2018, other more subtle gender differences exist. Women view
geotagging on Twitter more positively and surprisingly are less
concerned about privacy cite:Haffner2018. This aligns with Stephens'
cite:Stephens2013 observation that "Men are mapping," (in reference to
OSM), "and women are being mapped" (p. 994). 

While Web 2.0 is perhaps a more democratic form of content production,
it must be kept in mind that even emancipatory methods, like
participatory mapping, are embedded with their own hierarchical power
structures cite:Harris1995. The long tail effect, that is, the result
of a few users producing an excessive amount of content (SOURCE), must
be dealt with, though there is not a universally accepted way to do
so.Twitter "bot" accounts, which automate the production of tweets,
have the potential to conflate datasets and obscure processes if they
are not filtered appropriately (see Fig. 1). On the other hand, it is
not uncommon for a multitude of users within a study to have produced
only one or two geolocated tweets over long periods of time (e.g.
cite:Haffner2018a), calling into question the utility of such data.


** Computational challenges
*** How Twitter distributes data
Twitter makes data available in a number of different ways. It offers
several free APIs through which developers can access a limited
portion of streaming Twitter data in real time. Developers can
retrieve roughly a 1% random sample of all tweets, or they can filter
tweets by term or location. By using a four coordinate pair bounding
box around an area of interest, all geolocated tweets in the area can
be gathered. Though only a limited sample of past tweets are available
through the standard (i.e. free) API, premium and enterprise APIs,
offer access to all past tweets plus fewer (or no) restrictions on the
amount of streaming data that can be retrieved (SOURCE).

Yet even if data can be accessed, it cannot always be easily
processed, retrieved, or analyzed. The US Library of Congress
collected every tweet from Twitter's inception in 2006 until 2017,
with an estimated total of 170 billion unique messages
cite:LibraryOfCongress2017. In late 2017, however, the Library of
Congress announced that they would no longer continue to harvest these
data cite:LibraryOfCongress2017. Increasingly large amounts of data,
along with an expanding range of content (i.e. pictures and videos)
inhibited data collection procedures. Further, the Library has never
made the data available due to an inability to develop a suitable
distributional procedure. Such data handling challenges are common
today in Web 2.0's world of "big data", and the computational expense
incurred by spatial data makes handling geolocated social media data
particularly difficult.

*** Data handling

Social media data are more complex than tabular data. Each tweet can
return a variable number of keys and values representing the hashtags
used, another tweet that was quoted, users referenced, and links.
Twitter objects produced through the APIs can hundreds of different
keys present in some records and absent in others. This format, termed
"semi-structured", has challenged conventional forms of computing and
storage, particularly relational database management system (RDBMS)
model using Structured Query Language (SQL). Since platforms and data
can vary enormously, many different "Not only SQL" (NoSQL) data models
have emerged. These include a document store (as with MongoDB and
CouchDB), a wide column store (as with Cassandra), and a key-value
store (as with Riak and Redis) cite:Cattell2011. This lack of
standardization means that NoSQL principles cannot be ubiquitously
applied across systems, not to mention the technical aspects of
hands-on use. Additionally, the sheer volume of social media data
often requires distributed computing to efficiently extract results.
Using distributed resources with geolocated social media data
simultaneously requires knowledge of GIS and cyberinfrastructure, both
of which can change at fast rates cite:Wang2009.

Despite these challenges, several successful applications integrating
geolocated Twitter into a NoSQL framework have been described in
depth. Using MongoDB, Cao et al. cite:Cao2015 introduce a framework
for analyzing social media data across time and space. Developing a
system called FluMapper, they demonstrate a novel application that
synthesizes the travel patterns of potentially flu-infected Twitter
users. Similarly, Soltani et al. cite:Soltani2016 have developed a
system called UrbanFlow, which combines geotagged tweets with land use
parcels in the Chicago area. Such case studies can provide a useful
reference point for others, but subtle differences in requirements can
greatly change computing needs.

** Privacy
Privacy issues associated with location tagging are often only
mentioned in vague passing, barring a few, albeit notable, exceptions.
In their /GeoPrivacy Manifesto/, Kebler and McKenzie cite:Kebler2018
importantly point out that geoprivacy is difficult to assess and that
advancements in privacy are hard to assess. Zook et al. cite:Zook2017,
in developing /Ten simple rules for responsible big data research/,
importantly acknowledge that "data are people" (p. 2) and that privacy
is more complex than a simple binary state. While privacy issues most
often surface in /social/ applications of Twitter data, Crawford and
Finn cite:Crawford2015 importantly highlight the lack of privacy
discussions in the context of disaster situations. Though some
scenarios are time sensitive and represent life or death situations,
these data used are not immune to abuse, highlighting the need for
greater ethical scrutiny and critical perspectives on big data
regardless of its application.

Yet, corporations such as Google, Apple, Microsoft, and of course,
Twitter, have far greater detail about individuals and their
locational practices than the academy, which often relies on publicly
available data. Privacy breaches highlighted by the multi-billion
dollar litigation against Facebook have exposed the extent of
knowledge that corporations possess of individuals (SOURCE),  seemingly
confirming the fears of Miller and Goodchild cite:Miller2015 who
state, "We must be cognizant about where this research is occurring --
in the open light of scholarly research where peer review and
reproducibility is possible, or behind closed doors of private-sector
companies and government agencies, as proprietary products without
peer review and without full reproducibility" (p. 260). Knowing that
much research is indeed occurring outside of the light of peer review,
a question that logically follows is, what breaches of privacy have
occurred but will never become public?

Before the advent of the "data avalanche" cite:Miller2010, Dobson and
Fisher cite:Dobson2007 warned of the "panoptic" capabilities of human
location tracking. Such technologies are particularly dangerous when
they force the user to relinquish something (i.e. their privacy) while
simultaneously providing some benefit. Given that social media is being
adopted by an ever-increasing amount of the people cite:Smith2018 and that
many users view geotagging as an effective way to gain social capital
cite:Haffner2018, scrutiny of the way these data collected and used is
more important than ever.

* Twitter data for social disparity
Despite these drawbacks, analyses of geolocated Twitter data can be
powerful if the limitations are properly understood, particularly in
their ability to elicit patterns of social disparity. Uncovering
differences -- or simply absences -- in spatial patterns of production
has the capability of evoking geographic "digital divides"
cite:Warf2001. While lack of content in itself is not positive
evidence of disparity, online landscapes are reflective of material,
offline processes cite:Shelton2014. Going a step further, in the
information age -- where decisions about places are made via online
and social media presence (or a lack thereof) -- online content has
increasingly greater potential to affect the perception and
configuration of physical space.

Evaluating the Twitter data associated with Hurricane Sandy, Shelton
et al. cite:Shelton2014 find an expected association between areas
most affected by the Hurricane and the Twitter content referencing
Sandy, yet the correspondence is not one-to-one. They importantly
acknowledge that some locations might be neglected due to a lack in
material capabilities in producing content and that an analysis of
content /after/ a disaster could be effective in providing insight on
social inequality. Extending on this approach, Shelton et al.
cite:Shelton2015 find stark segregation between Louisville, Kentucky's
West End and East End Twitter users, not just within these
neighborhoods but also in the places throughout the metropolitan area
visited by both parties. Those who mostly produce tweets from the
more affluent East End rarely navigate the spaces visited by West End
users.

Taking a broader, place-based approach, Haffner cite:Haffner2018b
examines the spatial patterns and correlations between municipalties'
census data and their number of references to #BlackLivesMatter and
#AllLivesMatter in cities across Texas and Louisiana. Contrary to
commonly held notions that #AllLivesMatter is used as
counter-narrative to #BlackLivesMatter, there is a considerable
association between city's number of #AllLivesMatter references and
percent black. On the other hand, there is a strong negative
association between references to either phrase and percent white, and
the same holds true for percent Hispanic. This aligns with the notion
that whites are mostly unwilling to engage in thoughtful racial
dialogue, albeit in an unexpected way.

* Conclusion
This chapter has highlighted the history, applications, and challenges
of working with geolocated Twitter data. This history is marked with
tension in ways of conceptualizing the data, its value in light of its
biases, and how to handle such sources from a computational
perspective. Twitter's location tagging features are disabled by
default -- from the perspective of allowing other users to view the
data -- but the company uses IP addresses for geo-targeted
advertisements, complicating Twitter's "opt-in" verses "opt-out"
statuses. Further, even if a tweet is not formally geotagged, locational
information can still be extracted or inferred using other mechanisms.
Compared to other social media platforms, Twitter is more representative
in terms of its user base, but there is evidence suggesting that
various subgroups use Twitter in very different ways. Computational
stuff now?

Throughout its history, questions of privacy have been common, and
users practices on Twitter are constantly evolving and changing. That
said, there are strands of hope, and cases in which geolocated Twitter
data has been used from an emancipatory perspective.

Unexpected results, ability to tell a fuller story, provide more
information. 
* References
bibliographystyle:chicago
bibliography:~/Sync/references/references.bib
